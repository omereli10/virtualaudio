﻿using System;
using System.Collections.Generic;

namespace VirtualAudio
{
    public static class ProgramMenu
    {
        public static void CleanConsole()
        {
            Console.Clear();
            SpaceLine();
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine($"       VIRTUAL MIC AUDIO Xx_0MER_xX {{{Program.VERSION}}}");
            Console.WriteLine("--------------------------------------------------");
            SpaceLine();
            SpaceLine();
        }

        public static void SubTitle(string subTitle)
        {
            Console.WriteLine(subTitle);
            SpaceLine();
        }

        public static int ChooseInputDevice()
        {
            int userChoiceInputDevice;

            // Get the input devices
            List<string> inputAudioDevices = AudioDevicesManager.GetInputAudioDevices();

            // Write the input devices
            foreach (string inputAudioDevice in inputAudioDevices)
            {
                Console.WriteLine(inputAudioDevice);
            }

            // Make the user choose a device
            SpaceLine();
            Console.Write("Please choose your INPUT DEVICE by [Option Number]: ");
            userChoiceInputDevice = Int32.Parse(Console.ReadLine()) - 1;

            // While invalid ask again
            while (!AudioDevicesManager.IsValidInputAudioDevice(userChoiceInputDevice))
            {
                Console.WriteLine("INVALID OPTION NUMBER! Please choose again.");
                SpaceLine();
                SpaceLine();
                Console.Write("Please choose your INPUT DEVICE by [Option Number]: ");
                userChoiceInputDevice = Int32.Parse(Console.ReadLine()) - 1;
            }
            
            return userChoiceInputDevice;
        }

        public static int ChooseOutputDevice()
        {
            int userChoiceOutputDevice;

            // Get the output devices
            List<string> outputAudioDevices = AudioDevicesManager.GetOutputAudioDevices();

            // Write the output devices
            foreach (string outputAudioDevice in outputAudioDevices)
            {
                Console.WriteLine(outputAudioDevice);
            }

            // Make the user choose a device
            SpaceLine();
            Console.Write("Please choose your OUTPUT DEVICE by [Option Number]: ");
            userChoiceOutputDevice = Int32.Parse(Console.ReadLine()) - 1;

            // While invalid ask again
            while (!AudioDevicesManager.IsValidOutputAudioDevice(userChoiceOutputDevice))
            {
                Console.WriteLine("INVALID OPTION NUMBER! Please choose again.");
                SpaceLine();
                SpaceLine();
                Console.Write("Please choose your OUTPUT DEVICE by [Option Number]: ");
                userChoiceOutputDevice = Int32.Parse(Console.ReadLine()) - 1;
            }

            return userChoiceOutputDevice;
        }

        public static int ChooseBitRate()
        {
            int userChoiceBitRate = 0;

            // Write the bit rates
            for (int i = 0; i < AudioDevicesManager.BIT_RATE_OPTIONS.Length; i++)
            {
                Console.WriteLine($"[{i+1}] Bit Rate: {AudioDevicesManager.BIT_RATE_OPTIONS[i]}");
            }

            Console.Write("Please choose your BIT RATE by [Option Number]: ");
            userChoiceBitRate = Int32.Parse(Console.ReadLine()) - 1;

            // While invalid ask again
            while (!(userChoiceBitRate >= 0 && userChoiceBitRate < AudioDevicesManager.BIT_RATE_OPTIONS.Length))
            {
                Console.WriteLine("INVALID OPTION NUMBER! Please choose again.");
                SpaceLine();
                SpaceLine();
                Console.Write("Please choose your BIT RATE by [Option Number]: ");
                userChoiceBitRate = Int32.Parse(Console.ReadLine()) - 1;
            }

            return AudioDevicesManager.BIT_RATE_OPTIONS[userChoiceBitRate];
        }

        public static float ChooseVolume()
        {
            bool validVolume = false;
            float userChoiceVolume = 0;

            while (!validVolume)
            {
                try
                {
                    // Make the user choose a device
                    Console.Write("Please choose your VOLUME MULTIPLICATION (e.g. 2.0 is twice as loud): ");
                    userChoiceVolume = float.Parse(Console.ReadLine());

                    if (userChoiceVolume >= 0.0f)
                    {
                        validVolume = true;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
                catch
                {
                    Console.WriteLine("INVALID VOLUME OPTION! Please choose again.");
                    SpaceLine();
                }
            }

            return userChoiceVolume;
        }

        public static void Start(StreamAudio streamAudio)
        {
            Console.WriteLine("Starting...");
            streamAudio.Start();
            Console.WriteLine("STARTED SUCCESSFULLY!");
        }

        public static void WaitForCloseCommand()
        {
            Console.WriteLine("Press 3 enters stop...");
            Console.ReadLine();
            Console.ReadLine();
            Console.ReadLine();
        }

        public static void Stop(StreamAudio streamAudio)
        {
            Console.WriteLine("Stopping...");
            streamAudio.Stop();
            Console.WriteLine("STOPPED SUCCESSFULLY!");
        }

        public static void ShowInfo(params String[] infos)
        {
            foreach (String info in infos)
            {
                Console.WriteLine($"- [Info] {info}");
            }
        }

        public static void SpaceLine(params String[] infos)
        {
            Console.WriteLine();
        }

        public static void Exit()
        {
            System.Environment.Exit(1);
        }
    }
}
