﻿using NAudio.Wave;
using System.Collections.Generic;

namespace VirtualAudio
{
    public static class AudioDevicesManager
    {
        public static readonly int[] BIT_RATE_OPTIONS = new int[] { 48000, 44100 };

        public static List<string> GetInputAudioDevices()
        {
            List<string> inputAudioDevices = new List<string>();

            for (int i = 0; i < WaveIn.DeviceCount; i++)
            {
                inputAudioDevices.Add($"[{i+1}] Name: {{{WaveIn.GetCapabilities(i).ProductName}}}, Channels: {{{WaveIn.GetCapabilities(i).Channels}}}");
            }

            return inputAudioDevices;
        }

        public static List<string> GetOutputAudioDevices()
        {
            List<string> inputAudioDevices = new List<string>();

            for (int i = 0; i < WaveOut.DeviceCount; i++)
            {
                inputAudioDevices.Add($"[{i+1}] Name: {{{WaveOut.GetCapabilities(i).ProductName}}}, Channels: {{{WaveOut.GetCapabilities(i).Channels}}}");
            }

            return inputAudioDevices;
        }

        public static bool IsValidInputAudioDevice(int inputAudioDeviceNumber)
        {
            int deviceCount = WaveIn.DeviceCount;

            return inputAudioDeviceNumber >= 0 && inputAudioDeviceNumber < deviceCount;
        }

        public static bool IsValidOutputAudioDevice(int outputAudioDeviceNumber)
        {
            int deviceCount = WaveOut.DeviceCount;

            return outputAudioDeviceNumber >= 0 && outputAudioDeviceNumber < deviceCount;
        }
    }
}
