﻿using NAudio.Wave;

namespace VirtualAudio
{
    public class Program
    {
        public static readonly string VERSION = "1.1";

        public static void Main(string[] args)
        {
            int   inputDeviceNumber;
            int   outputDeviceNumber;
            int   bitRate;
            float volume;

            // Choose input device
            ProgramMenu.CleanConsole();
            ProgramMenu.SubTitle("INPUT DEVICE");
            inputDeviceNumber = ProgramMenu.ChooseInputDevice();

            // Choose output device
            ProgramMenu.CleanConsole();
            ProgramMenu.SubTitle("OUTPUT DEVICE");
            outputDeviceNumber = ProgramMenu.ChooseOutputDevice();

            // Choose bit rate
            ProgramMenu.CleanConsole();
            ProgramMenu.SubTitle("BIT RATE");
            bitRate = ProgramMenu.ChooseBitRate();

            // Choose volume
            ProgramMenu.CleanConsole();
            ProgramMenu.SubTitle("VOLUME");
            volume = ProgramMenu.ChooseVolume();

            // Start
            StreamAudio streamAudio = new StreamAudio(inputDeviceNumber, outputDeviceNumber, bitRate, volume);
            ProgramMenu.CleanConsole();
            ProgramMenu.Start(streamAudio);
            ProgramMenu.SpaceLine();
            ProgramMenu.ShowInfo($"Input device  : {WaveIn.GetCapabilities(inputDeviceNumber).ProductName}, Channels: {WaveIn.GetCapabilities(inputDeviceNumber).Channels}",
                                 $"Output device : {WaveOut.GetCapabilities(outputDeviceNumber).ProductName}, Channels: {WaveOut.GetCapabilities(outputDeviceNumber).Channels}",
                                 $"Bit Rate      : {bitRate}",
                                 $"Volume        : {volume}x");
            ProgramMenu.SpaceLine();

            // Wait until it closes
            ProgramMenu.WaitForCloseCommand();

            // Stop
            ProgramMenu.CleanConsole();
            ProgramMenu.Stop(streamAudio);

            // Exit
            ProgramMenu.Exit();
        }
    }
}