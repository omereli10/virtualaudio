﻿using NAudio.Wave;

namespace VirtualAudio
{
    public class StreamAudio
    {
        private static readonly int LATENCY  = 300;
        private float               _volume;
        private int                 _bitRate;
        private WaveInEvent         _waveIn  = null;
        private WaveOutEvent        _waveOut = null;
        private int                 _inputDeviceNumber;
        private int                 _outputDeviceNumber;

        public StreamAudio(int inputDeviceNumber, int outputDeviceNumber, int bitRate, float volume)
        {
            _inputDeviceNumber  = inputDeviceNumber;
            _outputDeviceNumber = outputDeviceNumber;
            _bitRate            = bitRate;
            _volume             = volume;
        }

        public void Start()
        {
            _waveIn = new WaveInEvent();
            _waveIn.DeviceNumber = _inputDeviceNumber;
            _waveIn.WaveFormat = new WaveFormat(_bitRate, WaveIn.GetCapabilities(_inputDeviceNumber).Channels);

            WaveInProvider waveInProvider = new WaveInProvider(_waveIn);
            VolumeWaveProvider16 volumeWave = new VolumeWaveProvider16(waveInProvider);
            volumeWave.Volume = _volume;

            _waveOut = new WaveOutEvent();
            _waveOut.DeviceNumber = _outputDeviceNumber;
            _waveOut.DesiredLatency = LATENCY;
            _waveOut.Init(volumeWave);

            _waveIn.StartRecording();
            _waveOut.Play();
        }

        public void Stop()
        {
            if (_waveOut != null)
            {
                _waveOut.Stop();
                _waveOut.Dispose();
                _waveOut = null;
            }

            if (_waveIn != null)
            {
                _waveIn.StopRecording();
                _waveIn.Dispose();
                _waveIn = null;
            }
        }
    }
}
